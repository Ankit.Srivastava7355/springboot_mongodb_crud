package com.example.MONGO.CRUD.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.MONGO.CRUD.entity.DepartmentSummary;
import com.example.MONGO.CRUD.response.ResponseHandler;
import com.example.MONGO.CRUD.serviceImpl.DepartmentSummaryServiceImpl;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;


@RestController
@RequestMapping("/api/v2")
public class DepartmentSummaryController {
	
	@Autowired
	DepartmentSummaryServiceImpl departmentsummaryserviceImpl;
	
	@GetMapping("/departmentsummary")
	public ResponseEntity<Object> GetDetails(@NotBlank(message="department string cannot be blank") @NotNull(message="department string cannot be null") @RequestParam("department") String department) {
		
        try {
        	return ResponseHandler.generateResponse("Department Summary Data fetched Successfully!", HttpStatus.OK, departmentsummaryserviceImpl.GetDetails(department));
		}
		catch(Exception e) {
			return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
		}
	}
}
