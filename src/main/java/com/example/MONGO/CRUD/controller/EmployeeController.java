package com.example.MONGO.CRUD.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.MONGO.CRUD.entity.Employee;
import com.example.MONGO.CRUD.response.ResponseHandler;
import com.example.MONGO.CRUD.serviceImpl.EmployeeServiceImpl;

import jakarta.validation.Valid;



@RestController
@RequestMapping("/api/v1")
public class EmployeeController {
	// Get all the employees present in the database
	@Autowired
	private EmployeeServiceImpl employeeserviceimpl;
	
	// get all the employees
	@GetMapping("/employees/getting")
	public ResponseEntity<Object> GetAllEmployees() {
		 try {
	        	return ResponseHandler.generateResponse("All Employees Data fetched Successfully!", HttpStatus.OK, employeeserviceimpl.getAllEmployees());
			}
			catch(Exception e) {
				return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
			}
		
	}
	
	// create an employee
	@PostMapping("/employees/adding")
	public ResponseEntity<Object> CreateAnEmployee(@Valid @RequestBody Employee employee) {
		 try {
	        	return ResponseHandler.generateResponse("Given Employee Added Successfully!", HttpStatus.OK, employeeserviceimpl.Create(employee));
			}
			catch(Exception e) {
				return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
			}	
	}
	
	// get a particular employee
	@GetMapping("employees/{id}")
	public ResponseEntity<Object> ReadAnEmployee( @PathVariable("id") String id) {
		 try {
	        	return ResponseHandler.generateResponse("Employee with given id fetched Successfully!", HttpStatus.OK, employeeserviceimpl.Read(id));
			}
			catch(Exception e) {
				return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
			}			
	}
	
	// updating an employee
	@PutMapping("employees/updating/{id}")
	public ResponseEntity<Object> UpdateAnEmployee(@PathVariable("id") String id,@Valid @RequestBody Employee employee)
	{
		 try {
	        	return ResponseHandler.generateResponse("Employee with given id updated Successfully!", HttpStatus.OK, employeeserviceimpl.Update(id, employee));
			}
			catch(Exception e) {
				return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
			}	
		
	}

	@DeleteMapping("employees/deleting/{id}")
	public ResponseEntity<Object> DeletingAnEmployee(@PathVariable("id") String id) {
		try {
        	return ResponseHandler.generateResponse("Employee with given id deleted Successfully!", HttpStatus.OK, employeeserviceimpl.Deleting(id));
		}
		catch(Exception e) {
			return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
		}	
 	}
}
