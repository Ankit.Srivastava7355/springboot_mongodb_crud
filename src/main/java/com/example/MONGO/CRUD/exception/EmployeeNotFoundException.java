package com.example.MONGO.CRUD.exception;

public class EmployeeNotFoundException extends Exception{
	 public EmployeeNotFoundException(String message) {
	        super(message);
	    }
}
