package com.example.MONGO.CRUD.exception;

public class DepartmentSummaryNotFound extends Exception{
	public DepartmentSummaryNotFound(String message) {
        super(message);
    }
}
