package com.example.MONGO.CRUD.service;

import java.util.List;

import com.example.MONGO.CRUD.entity.Employee;
import com.example.MONGO.CRUD.exception.EmployeeNotFoundException;


public interface EmployeeService {
	// create an employee
	public Employee Create(Employee employee);
	// update an employee 
	public Employee Update(String id,Employee employee) throws EmployeeNotFoundException;
	// read employee with given id
	public Employee Read(String id) throws EmployeeNotFoundException;
	// delete an employee 
	public String Deleting(String id) throws EmployeeNotFoundException;
	
	List<Employee> getAllEmployees();
}
