package com.example.MONGO.CRUD.service;

import com.example.MONGO.CRUD.entity.DepartmentSummary;
import com.example.MONGO.CRUD.exception.DepartmentSummaryNotFound;

public interface DepartmentSummaryService {
	// get details by department name;
	DepartmentSummary GetDetails(String department_name) throws DepartmentSummaryNotFound;
}
