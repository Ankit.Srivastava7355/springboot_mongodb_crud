package com.example.MONGO.CRUD.response;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ResponseHandler {

    public static ResponseEntity<Object> generateResponse(String message, HttpStatus status, Object data) {
        Map<String, Object> responseBody = new LinkedHashMap<>();
        responseBody.put("message", message);
        responseBody.put("status", status.value());
        responseBody.put("data", data);
        return new ResponseEntity<>(responseBody, status);
    }

    public static ResponseEntity<Object> generateErrorResponse(String message, HttpStatus status, String error) {
        Map<String, Object> responseBody = new LinkedHashMap<>();
        responseBody.put("message", message);
        responseBody.put("status", status.value());
        responseBody.put("error", error);
        return new ResponseEntity<>(responseBody, status);
    }

    public static ResponseEntity<Object> generateErrorResponse(HttpStatus status, String message) {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("status", status.value());
        responseMap.put("error", status.getReasonPhrase());
        responseMap.put("message", message);
        return new ResponseEntity<>(responseMap, status);
    }
}
