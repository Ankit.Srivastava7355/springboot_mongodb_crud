package com.example.MONGO.CRUD.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;


@Document(collection = "department")
public class DepartmentSummary implements Serializable{
	
    private static final long serialVersionUID = -3702735738609145758L;

	@Id
    private String id;
    
	@NotBlank
	@Size(max = 50)
	private String department;

	@NotNull
	@Min(0)
	private Long totalEmployees;

	@NotNull
	@DecimalMin("0.0")
	private Double totalSalary;

	@NotNull
	@DecimalMin("0.0")
	private Double averageExperience;

	@NotNull
	@DecimalMin("0.0")
	private Double averageSalary;

	@NotNull
	@DecimalMin("0.0")
	private Double maxSalary;

	@NotNull
	@DecimalMin("0.0")
	private Double minSalary;


	public DepartmentSummary(String id, String department, Long totalEmployees, Double totalSalary,
			Double averageExperience, Double averageSalary, Double maxSalary, Double minSalary) {
		super();
		this.id = id;
		this.department = department;
		this.totalEmployees = totalEmployees;
		this.totalSalary = totalSalary;
		this.averageExperience = averageExperience;
		this.averageSalary = averageSalary;
		this.maxSalary = maxSalary;
		this.minSalary = minSalary;
	}

	public DepartmentSummary(String department, Long totalEmployees, Double totalSalary, Double averageExperience,
			Double averageSalary, Double maxSalary, Double minSalary) {
		super();
		this.department = department;
		this.totalEmployees = totalEmployees;
		this.totalSalary = totalSalary;
		this.averageExperience = averageExperience;
		this.averageSalary = averageSalary;
		this.maxSalary = maxSalary;
		this.minSalary = minSalary;
	}

	public DepartmentSummary() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Long getTotalEmployees() {
		return totalEmployees;
	}

	public void setTotalEmployees(Long totalEmployees) {
		this.totalEmployees = totalEmployees;
	}

	public Double getTotalSalary() {
		return totalSalary;
	}

	public void setTotalSalary(Double totalSalary) {
		this.totalSalary = totalSalary;
	}

	public Double getAverageExperience() {
		return averageExperience;
	}

	public void setAverageExperience(Double averageExperience) {
		this.averageExperience = averageExperience;
	}

	public Double getAverageSalary() {
		return averageSalary;
	}

	public void setAverageSalary(Double averageSalary) {
		this.averageSalary = averageSalary;
	}

	public Double getMaxSalary() {
		return maxSalary;
	}

	public void setMaxSalary(Double maxSalary) {
		this.maxSalary = maxSalary;
	}

	public Double getMinSalary() {
		return minSalary;
	}

	public void setMinSalary(Double minSalary) {
		this.minSalary = minSalary;
	}

	@Override
	public String toString() {
		return "DepartmentSummary [id=" + id + ", department=" + department + ", totalEmployees=" + totalEmployees
				+ ", totalSalary=" + totalSalary + ", averageExperience=" + averageExperience + ", averageSalary="
				+ averageSalary + ", maxSalary=" + maxSalary + ", minSalary=" + minSalary + ", getId()=" + getId()
				+ ", getDepartment()=" + getDepartment() + ", getTotalEmployees()=" + getTotalEmployees()
				+ ", getTotalSalary()=" + getTotalSalary() + ", getAverageExperience()=" + getAverageExperience()
				+ ", getAverageSalary()=" + getAverageSalary() + ", getMaxSalary()=" + getMaxSalary()
				+ ", getMinSalary()=" + getMinSalary() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	   
    
}