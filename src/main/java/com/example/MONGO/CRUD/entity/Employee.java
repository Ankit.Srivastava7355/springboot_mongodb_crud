package com.example.MONGO.CRUD.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;

@Document(collection = "employee")
public class Employee implements Serializable{
    
	private static final long serialVersionUID = 1700761092759827407L;

	@Id
    private String id;
    
	@NotBlank(message="Please enter a department name")
    @NotNull(message = "Name cannot be null")
    @Pattern(regexp = "^[a-zA-Z ]*$", message = "Name should contain only alphabets and spaces")
    private String name;

    @NotNull(message = "Experience cannot be null")
    @Positive(message = "Experience should be a positive number")
    private Integer experience_in_years;

    
    @NotNull(message = "Salary cannot be null")
    @PositiveOrZero(message = "Salary should be a positive number or zero")
    private Double salary;

    @NotBlank(message="Please enter a department name")
    @NotNull(message = "Department cannot be null")
    @Pattern(regexp = "^[a-zA-Z]*$", message = "Department should contain only alphabets")
    private String department;


	public Employee(String id, String name, Integer experience_in_years, Double salary, String department) {
		super();
		this.id = id;
		this.name = name;
		this.experience_in_years = experience_in_years;
		this.salary = salary;
		this.department = department;
	}

	public Employee(String name, Integer experience_in_years, Double salary, String department) {
		super();
		this.name = name;
		this.experience_in_years = experience_in_years;
		this.salary = salary;
		this.department = department;
	}

	public Employee() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getExperience_in_years() {
		return experience_in_years;
	}

	public void setExperience_in_years(Integer experience_in_years) {
		this.experience_in_years = experience_in_years;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", experience_in_years=" + experience_in_years + ", salary="
				+ salary + ", department=" + department + ", getId()=" + getId() + ", getName()=" + getName()
				+ ", getExperience_in_years()=" + getExperience_in_years() + ", getSalary()=" + getSalary()
				+ ", getDepartment()=" + getDepartment() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

    
    
    
}
