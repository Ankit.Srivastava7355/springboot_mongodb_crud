package com.example.MONGO.CRUD.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.MONGO.CRUD.entity.DepartmentSummary;
import com.example.MONGO.CRUD.entity.Employee;
import com.example.MONGO.CRUD.exception.EmployeeNotFoundException;
import com.example.MONGO.CRUD.repository.DepartmentSummaryRepository;
import com.example.MONGO.CRUD.repository.EmployeeRepository;
import com.example.MONGO.CRUD.service.EmployeeService;


@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Autowired
    private DepartmentSummaryRepository departmentSummaryRepository;


    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    private void updateDepartmentSummary(String department) {
        List<Employee> employees = employeeRepository.findByDepartment(department);
        DepartmentSummary departmentSummary = departmentSummaryRepository.findByDepartment(department);
        if (departmentSummary == null) {
            departmentSummary = new DepartmentSummary();
            departmentSummary.setDepartment(department);
        }
        departmentSummary.setTotalEmployees((long) employees.size());
        Double totalSalary = 0.0;
        Double maxSalary = Double.MIN_VALUE;
        Double minSalary = Double.MAX_VALUE;
        for (Employee employee : employees) {
            totalSalary += employee.getSalary();
            if (employee.getSalary() > maxSalary) {
                maxSalary = employee.getSalary();
            }
            if (employee.getSalary() < minSalary) {
                minSalary = employee.getSalary();
            }
        }
        departmentSummary.setTotalSalary(totalSalary);
        if (employees.size() > 0) {
            departmentSummary.setAverageExperience((double) employees.stream().mapToDouble(Employee::getExperience_in_years).average().getAsDouble());
            departmentSummary.setAverageSalary(totalSalary / employees.size());
        } else {
            departmentSummary.setAverageExperience(0.0);
            departmentSummary.setAverageSalary(0.0);
        }
        departmentSummary.setMaxSalary(maxSalary);
        departmentSummary.setMinSalary(minSalary);
        departmentSummaryRepository.save(departmentSummary);
    }

	@Override
	public Employee Create(Employee employee) {
	    Employee savedEmployee = employeeRepository.save(employee);
        updateDepartmentSummary(savedEmployee.getDepartment());
        return savedEmployee;
	}

	@Override
	public Employee Update(String id, Employee employee) throws EmployeeNotFoundException {
		  Employee existingEmployee = employeeRepository.findById(id).orElse(null);
		    if (existingEmployee == null) {
		    	throw new EmployeeNotFoundException("Employee not found with id : "+id);
		    }
		    String oldDepartment = existingEmployee.getDepartment();
		    existingEmployee.setName(employee.getName());
		    existingEmployee.setExperience_in_years(employee.getExperience_in_years());
		    existingEmployee.setSalary(employee.getSalary());
		    existingEmployee.setDepartment(employee.getDepartment());
		    Employee updatedEmployee = employeeRepository.save(existingEmployee);
		    if (!oldDepartment.equals(updatedEmployee.getDepartment())) {
		        updateDepartmentSummary(oldDepartment);
		        updateDepartmentSummary(updatedEmployee.getDepartment());
		    } else {
		        updateDepartmentSummary(updatedEmployee.getDepartment());
		    }
		    return updatedEmployee;
	}

	@Override
	public Employee Read(String id) throws EmployeeNotFoundException {
        Employee uu = employeeRepository.findById(id).orElse(null);
        if(uu==null) {
        	throw new EmployeeNotFoundException("Employee not found with id : "+id);
        }
        else {
        	return uu;
        }
	}

	@Override
	public String Deleting(String id) throws EmployeeNotFoundException {
		 Employee existingEmployee = employeeRepository.findById(id).orElse(null);
	        if (existingEmployee != null) {
	            employeeRepository.delete(existingEmployee);
	            updateDepartmentSummary(existingEmployee.getDepartment());
	            return "Employee with given id deleted successfully";
	        }
	        else {
	           	throw new EmployeeNotFoundException("Employee not found with id : "+id);
	        }
	}
}

