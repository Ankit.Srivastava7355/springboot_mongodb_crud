package com.example.MONGO.CRUD.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import com.example.MONGO.CRUD.entity.DepartmentSummary;
import com.example.MONGO.CRUD.exception.DepartmentSummaryNotFound;
import com.example.MONGO.CRUD.repository.DepartmentSummaryRepository;
import com.example.MONGO.CRUD.service.DepartmentSummaryService;

@Service
public class DepartmentSummaryServiceImpl implements DepartmentSummaryService{

	@Autowired
	DepartmentSummaryRepository departmentSummaryRepository;

	@Override
	public DepartmentSummary GetDetails(String departmentName) throws DepartmentSummaryNotFound {
		DepartmentSummary gg = departmentSummaryRepository.findByDepartment(departmentName);
		if(gg==null) {
			throw new DepartmentSummaryNotFound("The given department= "+departmentName+ " not found !");
		}
		else{
			return gg;
		}
	}

}
