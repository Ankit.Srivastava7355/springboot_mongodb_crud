package com.example.MONGO.CRUD.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.MONGO.CRUD.entity.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, String>{

	List<Employee> findByDepartment(String department);

}