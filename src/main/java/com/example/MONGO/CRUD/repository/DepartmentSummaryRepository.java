package com.example.MONGO.CRUD.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.MONGO.CRUD.entity.DepartmentSummary;

@Repository
public interface DepartmentSummaryRepository extends MongoRepository<DepartmentSummary, String> {

    DepartmentSummary findByDepartment(String department);

}
