# SpringBoot_MongoDB_CRUD_REST_APIs
5th assignment :

API should perform CRUD using spring boot , mongo

mongo employee & department_summary collection should be pre created before project .

employee info should be store in employee collection .

store the summary on department wise for employee in department_summary collection.

we need another API to get the summary of department . input will be department_name .

API should have validations on strict types and compulsory parameters and it should return proper error codes & error messages .

# REST API END POINTS : 

```
Get all the employees present in the database -> GET => http://localhost:8081/api/v1/employees/getting
Create an employee -> POST => http://localhost:8081/api/v1/employees/adding
Update the employee with given id -> PUT => http://localhost:8081/api/v1/employees/updating/6419826accea7d7b643c24ec
Fetch the employee with the given id -> GET => http://localhost:8081/api/v1/employees/641d947f10052a28c8d87487
Delete the employee from the database with the given id -> DELETE => http://localhost:8081/api/v1/employees/deleting/641d94ff10052a28c8d87489
Get the department wise summary for a given department through this end point -> GET => http://localhost:8081/api/v2/departmentsummary?department=
```